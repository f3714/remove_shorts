# Remove_Shorts(by NOface312)
This script based by https://greasyfork.org/en/scripts/442329-remove-youtube-shorts

## What it does
Working on Desktop and mobile
- Remove Youtube Shorts(Delete Videos, Links, Buttons in menu)
- Redirect to the home page if you go to shorts
- Remove YouTube Recommendations(Only PC)

## GitLab Link
Source code link https://gitlab.com/f3714/remove_shorts

## Getting started
There will be a button next to the Search bar (Only PC in mobile version script working always).
Green = ON
Red = OFF

Your choice will be stored if you open a new tab, refresh the page or close the browser.

You can change how the script works with the config variable.

## Important
Since the script runs every second or half a second, it wastes a lot of RAM (about 300mb or more). Be careful or you can increase the interval of the script